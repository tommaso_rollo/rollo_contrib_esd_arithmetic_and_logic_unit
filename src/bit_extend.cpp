#include <systemc.h>
#include "bit_extend.hpp"

using namespace std;

void bit_extend::bit_extend_perform() {
	
	sc_uint<16> bit_extend_in_temp, zeros;	
	sc_uint<32> bit_extend_result;
	
	zeros = 0;

	while(true) {
		
		wait();
		bit_extend_in_temp = bit_extend_in->read();
		bit_extend_result = (zeros, bit_extend_in_temp);
	
		bit_extend_out->write(bit_extend_result);
		
 	}   

}
   
