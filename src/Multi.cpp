// 16bit input Multiplier
#include <systemc.h>
#include <math.h>
#include "Multi.hpp"

using namespace std;

void Multi::Multi_operate(){
	
	sc_uint<2*NBIT> temp_out;

	while(true){
		wait();
		
		temp_out = Multi_input1->read() * Multi_input2->read();

		Multi_output->write(temp_out);	
	}
}
