#include <systemc.h>
#include "alu2.hpp"

using namespace std;

SC_HAS_PROCESS(alu2);

alu2::alu2(sc_module_name name): 
	sc_module(name), 
        add1("add1"), multi1("multi1"), comp1("comp1"), and1("and1"), nand1("nand1"), or1("add1"), nor1("comp1"), 
        xor1("and1"), xnor1("nand1"), not1("not1"), not2("not2"), shift1("shift1"),shift2("shift2"), 
        mux16_1("mux16_1"), be1("be1"), be2("be2"), be3("be3"), be4("be4"), be5("be5"), be6("be6"), be7("be7"), be8("be8"), be9("be9"), be10("be10"), 
	be11("be11"), be12("be12") {

  zeros = 0;
  shift_amount = 1;

  // Port Map
  comp1.Comp_input1(alu_in1);
  comp1.Comp_input2(alu_in2);
  comp1.Comp_output(comp1_out);
  be1.bit_extend_in(comp1_out);
  be1.bit_extend_out(comp1_out2);

  multi1.Multi_input1(alu_in1);
  multi1.Multi_input2(alu_in2);
  multi1.Multi_output(multi1_out);

  and1.and_gate_in1(alu_in1);
  and1.and_gate_in2(alu_in2);
  and1.and_gate_out(and1_out);
  be2.bit_extend_in(and1_out);
  be2.bit_extend_out(and1_out2);

  or1.or_gate_in1(alu_in1);
  or1.or_gate_in2(alu_in2);
  or1.or_gate_out(or1_out);
  be3.bit_extend_in(or1_out);
  be3.bit_extend_out(or1_out2);

  not1.NOT_input(alu_in1);
  not1.NOT_output(not1_out);
  be4.bit_extend_in(not1_out);
  be4.bit_extend_out(not1_out2);
  
  not2.NOT_input(alu_in2);
  not2.NOT_output(not2_out);
  be5.bit_extend_in(not2_out);
  be5.bit_extend_out(not2_out2);

  xor1.xor_gate_in1(alu_in1);
  xor1.xor_gate_in2(alu_in2);
  xor1.xor_gate_out(xor1_out);
  be6.bit_extend_in(xor1_out);
  be6.bit_extend_out(xor1_out2);

  nand1.nand_gate_in1(alu_in1);
  nand1.nand_gate_in2(alu_in2);
  nand1.nand_gate_out(nand1_out);
  be7.bit_extend_in(nand1_out);
  be7.bit_extend_out(nand1_out2);
  
  nor1.nor_gate_in1(alu_in1);
  nor1.nor_gate_in2(alu_in2);
  nor1.nor_gate_out(nor1_out);
  be8.bit_extend_in(nor1_out);
  be8.bit_extend_out(nor1_out2);
  
  xnor1.xnor_gate_in1(alu_in1);
  xnor1.xnor_gate_in2(alu_in2);
  xnor1.xnor_gate_out(xnor1_out);
  be9.bit_extend_in(xnor1_out);
  be9.bit_extend_out(xnor1_out2);

  add1.add_incr_in1(alu_in1);
  add1.add_incr_in2(alu_in2);
  add1.add_incr_op1(alu_sel_add1);
  add1.add_incr_op2(alu_sel_add2);
  add1.add_incr_out(adder1_out);
  be10.bit_extend_in(adder1_out);
  be10.bit_extend_out(adder1_out2);
  
  shift1.SR_input(alu_in1);
  shift1.quantity(shift_amount);
  shift1.sel(alu_shift);
  shift1.SR_output(shift1_out);
  be11.bit_extend_in(shift1_out);
  be11.bit_extend_out(shift1_out2);

  shift2.SR_input(alu_in2);
  shift2.quantity(shift_amount);
  shift2.sel(alu_shift);
  shift2.SR_output(shift2_out);
  be12.bit_extend_in(shift2_out);
  be12.bit_extend_out(shift2_out2);

  mux16_1.mux16_in1(comp1_out2);
  mux16_1.mux16_in2(multi1_out);
  mux16_1.mux16_in3(and1_out2);
  mux16_1.mux16_in4(or1_out2);
  mux16_1.mux16_in5(not1_out2);
  mux16_1.mux16_in6(not2_out2);
  mux16_1.mux16_in7(xor1_out2);
  mux16_1.mux16_in8(nand1_out2);
  mux16_1.mux16_in9(nor1_out2);
  mux16_1.mux16_in10(xnor1_out2);
  mux16_1.mux16_in11(adder1_out2);
  mux16_1.mux16_in12(shift1_out2);
  mux16_1.mux16_in13(shift2_out2);
  mux16_1.mux16_in14(zeros);
  mux16_1.mux16_in15(zeros);
  mux16_1.mux16_in16(zeros);
  mux16_1.mux16_sel(alu_operation);
  mux16_1.mux16_out(alu_out);


}


