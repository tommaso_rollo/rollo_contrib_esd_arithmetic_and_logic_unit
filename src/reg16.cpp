#include <systemc.h>
#include "reg16.hpp"

using namespace std;

void reg16::reg16_perform() {
	
	sc_uint<16> reg_temp;
	bool load_temp;
	reg_temp = 0;
	while(true) {
		wait();
		load_temp = reg16_load->read();
		
		if(load_temp) {
			{
				reg_temp = reg16_in->read();		
				reg16_out->write(reg_temp);
			}
		}

				
				
 	}   

}
   
