// 16bit Comparator
#include <systemc.h>
#include <math.h>
#include "Comp.hpp"

using namespace std;

void Comp::Comp_operate(){
	
	while(true){
		wait();
		
		if(Comp_input1->read() == Comp_input2->read()) 
		{
			Comp_output->write(1);
			
		}
		else 
			Comp_output->write(0);
			
		
	}
}
