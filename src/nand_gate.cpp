#include <systemc.h>
#include "nand_gate.hpp"

using namespace std;

void nand_gate::nand_gate_perform() {
	
	sc_uint<16> nand_gate_in1_temp, nand_gate_in2_temp;	
	sc_uint<16> nand_gate_result;

	while(true) {
		
		wait();
		nand_gate_in1_temp = nand_gate_in1->read();
		nand_gate_in2_temp = nand_gate_in2->read();			
		for (unsigned i=0; i<16; i++) {
        		nand_gate_result[i] = ~(nand_gate_in1_temp[i] & nand_gate_in2_temp[i]);		
		}
	
	nand_gate_out->write(nand_gate_result);
		
 	}   

}
   
