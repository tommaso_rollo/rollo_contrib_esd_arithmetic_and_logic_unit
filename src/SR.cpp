// Shifter
#include <systemc.h>
#include "SR.hpp"

using namespace std;

void SR::SR_operate(){
	
	sc_uint<NBIT> temp_in;
	
        while(true){
		wait();
		temp_in = SR_input->read();
	
		if(sel->read() == 0){	//left shift: x2
			SR_output->write(temp_in << quantity->read());}
		else if(sel->read() == 1)
		{	SR_output->write(temp_in >> quantity->read());}
		
	}
}
