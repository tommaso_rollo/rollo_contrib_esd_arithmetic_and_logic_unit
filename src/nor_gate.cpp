#include <systemc.h>
#include "nor_gate.hpp"

using namespace std;

void nor_gate::nor_gate_perform() {
	
	sc_uint<16> nor_gate_in1_temp, nor_gate_in2_temp;	
	sc_uint<16> nor_gate_result;

	while(true) {
		
		wait();
		nor_gate_in1_temp = nor_gate_in1->read();
		nor_gate_in2_temp = nor_gate_in2->read();			
		for (unsigned i=0; i<16; i++) {
        		nor_gate_result[i] = ~(nor_gate_in1_temp[i] | nor_gate_in2_temp[i]);		
		}
	
	nor_gate_out->write(nor_gate_result);
		
 	}   

}
   
