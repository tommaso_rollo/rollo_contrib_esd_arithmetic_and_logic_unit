#include <systemc.h>
#include "alu.hpp"

using namespace std;

SC_HAS_PROCESS(alu);

alu::alu(sc_module_name name): 
	sc_module(name), 
        add1("add1"), comp1("comp1"), and1("and1"), nand1("nand1"),
	or1("add1"), nor1("comp1"), xor1("and1"), xnor1("nand1"),
	mux8_1("mux8_1") {

  // Port Map
  add1.adder_in1(alu_in1);
  add1.adder_in2(alu_in2);
  add1.adder_out(add1_out);
  
  comp1.comparator_in1(alu_in1);
  comp1.comparator_in2(alu_in2);
  comp1.comparator_out(comp1_out);
  
  and1.and_gate_in1(alu_in1);
  and1.and_gate_in2(alu_in2);
  and1.and_gate_out(and1_out);
  
  nand1.nand_gate_in1(alu_in1);
  nand1.nand_gate_in2(alu_in2);
  nand1.nand_gate_out(nand1_out);
  
  or1.or_gate_in1(alu_in1);
  or1.or_gate_in2(alu_in2);
  or1.or_gate_out(or1_out);
  
  nor1.nor_gate_in1(alu_in1);
  nor1.nor_gate_in2(alu_in2);
  nor1.nor_gate_out(nor1_out);
  
  xor1.xor_gate_in1(alu_in1);
  xor1.xor_gate_in2(alu_in2);
  xor1.xor_gate_out(xor1_out);
  
  xnor1.xnor_gate_in1(alu_in1);
  xnor1.xnor_gate_in2(alu_in2);
  xnor1.xnor_gate_out(xnor1_out);

  mux8_1.mux8_in1(add1_out);
  mux8_1.mux8_in2(comp1_out);
  mux8_1.mux8_in3(and1_out);
  mux8_1.mux8_in4(nand1_out);
  mux8_1.mux8_in5(or1_out);
  mux8_1.mux8_in6(nor1_out);
  mux8_1.mux8_in7(xor1_out);
  mux8_1.mux8_in8(xnor1_out);
  mux8_1.mux8_sel(alu_op);
  mux8_1.mux8_out(alu_out);

}


