#include <systemc.h>
#include "xor_gate.hpp"

using namespace std;

void xor_gate::xor_gate_perform() {
	
	sc_uint<16> xor_gate_in1_temp, xor_gate_in2_temp;	
	sc_uint<16> xor_gate_result;

	while(true) {
		
		wait();
		xor_gate_in1_temp = xor_gate_in1->read();
		xor_gate_in2_temp = xor_gate_in2->read();			
		for (unsigned i=0; i<16; i++) {
        		xor_gate_result[i] = (xor_gate_in1_temp[i] ^ xor_gate_in2_temp[i]);		
		}
	
	xor_gate_out->write(xor_gate_result);
		
 	}   

}
   
