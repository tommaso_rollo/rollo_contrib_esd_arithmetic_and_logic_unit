#include <systemc.h>
#include "reg32.hpp"

using namespace std;

void reg32::reg32_perform() {
	
	sc_uint<16> reg_temp;
	bool load_temp, clk_temp;

	while(true) {
		wait();
		reg_temp = reg32_in->read();		
		load_temp = reg32_load->read();
		
		if(load_temp) {
			{
				reg32_out->write(reg_temp);
			}
		}
				
				
 	}   

}
   
