#include <systemc.h>
#include <string>
#include "reg16.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<16> >	tb_in;
	sc_signal<sc_uint<16> > tb_out;
	sc_signal<bool> tb_clk,tb_load;
	
	reg16	tb_reg16;
	
	SC_CTOR(TB):tb_reg16("tb_reg16"){
                SC_THREAD(clock_gen);
       		SC_THREAD(testing);
		tb_reg16.reg16_in(this -> tb_in);
		tb_reg16.reg16_load(this -> tb_load);
		tb_reg16.reg16_clk(this -> tb_clk);
		tb_reg16.reg16_out(this -> tb_out);
		
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 4;
	unsigned vtest_in[TSIZE], vtest_out[TSIZE];
	bool load_values[TSIZE];
	//unsigned clock_count;
    	unsigned clock_period;
    
	void clock_gen() {

     		bool value = false;
     		while(true) {
            		tb_clk.write(value);
            		value = !value;
            		//clock_count++;
            		wait(clock_period/2,SC_NS);
      		}
   	}

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{

			// input test
			tb_in.write(vtest_in[j]);
			cout << "****************" << endl;	
			cout << "Register input: " << vtest_in[j] << endl;
			tb_load.write(load_values[j]);
			cout << "Register load: " << load_values[j] << endl;
			wait(clock_period,SC_NS); 
           		cout << "****************" << endl;	
			vtest_out[j] = tb_out.read();
			cout << "Register output: "<< vtest_out[j] << endl;
			cout << "****************" << endl;	
			cout << "****************" << endl;	
			cout << " " << endl;	
			}
	}
	
	// set test values	
	void set_vtest(){


 		//Inizializzazione del contatore dei cicli di clock
   		//clock_count=0;
    		//Tclk [ns]
       		clock_period=20;

		load_values[0] = 1;
		load_values[1] = 1;
		load_values[2] = 1;
		load_values[3] = 1;


		vtest_in[0] = 10;
		vtest_in[1] = 5;
		vtest_in[2] = 16;
		vtest_in[3] = 7;
		
		}
};

int sc_main(int argc, char* argv[])
{
  TB test("test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;
  
  sc_start(200,SC_NS);
  
  
  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
