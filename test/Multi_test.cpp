#include <systemc.h>
#include <string>
#include <math.h>
#include "Multi.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<NBIT> >	input1;
	sc_signal<sc_uint<NBIT> >	input2;
	sc_signal<sc_uint<2*NBIT> > 	output;
	
	Multi tb_Multi;
	
	SC_CTOR(TB):tb_Multi("tb_Multi"){
		SC_THREAD(testing);
		tb_Multi.Multi_input1(this -> input1);
		tb_Multi.Multi_input2(this -> input2);
		tb_Multi.Multi_output(this -> output);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE], vtest_out[TSIZE]; 

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;		
			input1.write(vtest_in1[j]);
			cout << "Multiplier first input: " << vtest_in1[j] <<endl;
			input2.write(vtest_in2[j]);
			cout << "Multiplier second input: " << vtest_in2[j] <<endl<<endl;
	
			wait(2,SC_NS);	// 2 ns

			vtest_out[j] = output.read();
			cout << "Multiplier output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 12;
		vtest_in2[0] = 12;
		
		vtest_in1[1] = pow(2,NBIT)-1;	// 2^NBIT-1
		vtest_in2[1] = pow(2,NBIT)-1; }
};

int sc_main(int argc, char* argv[])
{
  TB test("Multi test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
