#include <systemc.h>
#include <string>
#include "SR.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<NBIT> > input1;
	sc_signal<sc_uint<Q> > qt;
	sc_signal<bool> select;
	sc_signal<sc_uint<NBIT> > output1;
	
	SR	tb_SR;
	
	SC_CTOR(TB):tb_SR("tb_SR"){
		SC_THREAD(testing);
		tb_SR.SR_input(this -> input1);
		tb_SR.quantity(this -> qt);
		tb_SR.sel(this -> select);
		tb_SR.SR_output(this -> output1);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 2;
	//sc_lv<NBIT> vtest_in[TSIZE], vtest_out[TSIZE];
	unsigned vtest_in[TSIZE], vtest_out[TSIZE],vtest_qt[TSIZE];
	unsigned vtest_sel[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;
			cout << endl << " sel=0: left, sel=1: right" <<endl;
			input1.write(vtest_in[j]);
			cout << "Shifter input: " << vtest_in[j] <<endl;
			qt.write(vtest_qt[j]);
			cout << "Shift quantity: " << vtest_qt[j] <<endl;
			select.write(vtest_sel[j]);
			cout << "Select input: "<< vtest_sel[j] <<endl<<endl;

			wait(2,SC_NS);	// 2 ns

			vtest_out[j] = output1.read();
			cout << "SR output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in[0] = 128;
		vtest_sel[0] = 1;
		vtest_qt[0] = 1;

		vtest_in[1] = 256;
		vtest_sel[1] = 0; 
		vtest_qt[1] = 2;
}
};

int sc_main(int argc, char* argv[])
{
  TB test("test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}

