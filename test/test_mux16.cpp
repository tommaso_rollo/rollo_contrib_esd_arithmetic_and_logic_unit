#include <systemc.h>
#include <string>
#include "mux16.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<32> > op1, op2, op3, op4, op5, op6, op7, op8, op9, op10, op11, op12, op13, op14, op15, op16;
    sc_signal<sc_uint<4> > tb_sel;
    sc_signal<sc_uint<32> > res;
    mux16 tb_mux16;
    
    SC_CTOR(TestBench) : tb_mux16("tb_mux16")
    {
        SC_THREAD(stimulus_thread);
        tb_mux16.mux16_in1(this->op1);
        tb_mux16.mux16_in2(this->op2);
        tb_mux16.mux16_in3(this->op3);
        tb_mux16.mux16_in4(this->op4);
        tb_mux16.mux16_in5(this->op5);
        tb_mux16.mux16_in6(this->op6);
        tb_mux16.mux16_in7(this->op7);
        tb_mux16.mux16_in8(this->op8);
        tb_mux16.mux16_in9(this->op9);
        tb_mux16.mux16_in10(this->op10);
        tb_mux16.mux16_in11(this->op11);
        tb_mux16.mux16_in12(this->op12);
        tb_mux16.mux16_in13(this->op13);
        tb_mux16.mux16_in14(this->op14);
        tb_mux16.mux16_in15(this->op15);
        tb_mux16.mux16_in16(this->op16);
        tb_mux16.mux16_sel(this->tb_sel);
        tb_mux16.mux16_out(this->res);
        init_values();
    }

    

  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            op1.write(op1_values[i]);
            cout << "1st Operand: " << op1_values[i] << endl;
            op2.write(op2_values[i]);
            cout << "2nd Operand: " << op2_values[i] << endl;
	    op3.write(op3_values[i]);
            cout << "3rd Operand: " << op3_values[i] << endl;
            op4.write(op4_values[i]);
            cout << "4th Operand: " << op4_values[i] << endl;
	    op5.write(op5_values[i]);
            cout << "5h Operand: " << op5_values[i] << endl;
            op6.write(op6_values[i]);
            cout << "6th Operand: " << op6_values[i] << endl;
	    op7.write(op7_values[i]);
            cout << "7th Operand: " << op7_values[i] << endl;
            op8.write(op8_values[i]);
            cout << "8th Operand: " << op8_values[i] << endl;
	    op9.write(op9_values[i]);
            cout << "9th Operand: " << op9_values[i] << endl;
            op10.write(op10_values[i]);
            cout << "10th Operand: " << op10_values[i] << endl;
	    op11.write(op11_values[i]);
            cout << "11th Operand: " << op11_values[i] << endl;
            op12.write(op12_values[i]);
            cout << "12th Operand: " << op12_values[i] << endl;
	    op13.write(op13_values[i]);
            cout << "13th Operand: " << op13_values[i] << endl;
            op14.write(op14_values[i]);
            cout << "14th Operand: " << op14_values[i] << endl;
	    op15.write(op15_values[i]);
            cout << "15th Operand: " << op15_values[i] << endl;
            op16.write(op16_values[i]);
            cout << "16th Operand: " << op16_values[i] << endl;
	    tb_sel.write(tb_sel_values[i]);
            cout << "Mux Select (0 to 15): " << tb_sel_values[i] << endl;
	    wait(2, SC_NS);
            result_computed[i] = res.read(); 
            cout << "MUX out: " << result_computed[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 2;
    
    unsigned op1_values[TEST_SIZE];
    unsigned op2_values[TEST_SIZE];
    unsigned op3_values[TEST_SIZE];
    unsigned op4_values[TEST_SIZE];
    unsigned op5_values[TEST_SIZE];
    unsigned op6_values[TEST_SIZE];
    unsigned op7_values[TEST_SIZE];
    unsigned op8_values[TEST_SIZE];
    unsigned op9_values[TEST_SIZE];
    unsigned op10_values[TEST_SIZE];
    unsigned op11_values[TEST_SIZE];
    unsigned op12_values[TEST_SIZE];
    unsigned op13_values[TEST_SIZE];
    unsigned op14_values[TEST_SIZE];
    unsigned op15_values[TEST_SIZE];
    unsigned op16_values[TEST_SIZE];
    unsigned tb_sel_values[TEST_SIZE];
    unsigned result_computed[TEST_SIZE];

    void init_values() 
    {
        op1_values[0] = 4;
        op1_values[1] = 27;
        
        
        op2_values[0] = 13;
        op2_values[1] = 33;


        op3_values[0] = 1;
        op3_values[1] = 16;
        
        
        op4_values[0] = 256;
        op4_values[1] = 1024;

	
        op5_values[0] = 417;
        op5_values[1] = 326;
        
        
        op6_values[0] = 134;
        op6_values[1] = 65;

	
        op7_values[0] = 97;
        op7_values[1] = 128;
        
        
        op8_values[0] = 2;
        op8_values[1] = 8;
	

        op9_values[0] = 4;
        op9_values[1] = 27;
        
        
        op10_values[0] = 13;
        op10_values[1] = 33;


        op11_values[0] = 1;
        op11_values[1] = 16;
        
        
        op12_values[0] = 256;
        op12_values[1] = 1024;

	
        op13_values[0] = 417;
        op13_values[1] = 326;
        
        
        op14_values[0] = 134;
        op14_values[1] = 65;

	
        op15_values[0] = 97;
        op15_values[1] = 128;
        
        
        op16_values[0] = 2;
        op16_values[1] = 8;
	

	tb_sel_values[0] = 2;
        tb_sel_values[1] = 14;
	
	
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "..TEST IN PROGRESS.." << endl << endl;

  sc_start();

  return 0;
}
