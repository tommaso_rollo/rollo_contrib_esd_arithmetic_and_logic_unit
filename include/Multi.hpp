// 16bit input Multiplier

#ifndef Multi_HPP
#define Multi_HPP

#define NBIT 16

SC_MODULE(Multi){
	
	sc_in<sc_uint<NBIT> > Multi_input1;
	sc_in<sc_uint<NBIT> > Multi_input2;

	sc_out<sc_uint<2*NBIT> > Multi_output;

	SC_CTOR(Multi){
		SC_THREAD(Multi_operate);
		sensitive << Multi_input1 << Multi_input2;}

	void Multi_operate();
};

#endif
