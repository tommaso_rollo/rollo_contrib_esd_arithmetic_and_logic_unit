#ifndef mux8_HPP
#define mux8_HPP

SC_MODULE(mux8) {
  
  sc_in<sc_uint<16> > mux8_in1, mux8_in2, mux8_in3, mux8_in4, mux8_in5, mux8_in6, mux8_in7, mux8_in8;
  sc_in<sc_uint<3> > mux8_sel;
  sc_out<sc_uint<16> > mux8_out;
  
  

  SC_CTOR(mux8) {
	SC_THREAD(mux8_perform);     
	sensitive << mux8_in1 << mux8_in2 << mux8_in3 << mux8_in4 << mux8_in5 << mux8_in6 << mux8_in7 << mux8_in8 << mux8_sel;
     }

  void mux8_perform();

};

#endif
