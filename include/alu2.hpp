#ifndef ALU2_HPP
#define ALU2_HPP

#include "adder.hpp"		//
#include "mux2.hpp"		//
#include "add_incr.hpp"	// ARITHMETIC 
#include "Multi.hpp"		//
#include "bit_extend.hpp"		//
#include "Comp.hpp"	// and LOGIC	
#include "SR.hpp"		//
#include "and_gate.hpp"	        //
#include "or_gate.hpp"		//	
#include "nand_gate.hpp"	//
#include "nor_gate.hpp"		//
#include "NOT_gate.hpp"		//
#include "xor_gate.hpp"		//
#include "xnor_gate.hpp"	// UNIT

#include "mux16.hpp"	// MUX to select the output


using namespace std;


SC_MODULE(alu2) {
  
  // ALU entity
  sc_in<sc_uint<16> > alu_in1, alu_in2;
  sc_in<bool>  alu_sel_add1, alu_sel_add2, alu_shift;
  sc_in<sc_uint<4> >  alu_operation;
  sc_out<sc_uint<32> > alu_out;


  // Internal signals
  sc_signal<sc_uint<4> > shift_amount;
  sc_signal<sc_uint<16> > comp1_out, and1_out, nand1_out, or1_out, nor1_out, xor1_out, xnor1_out, not1_out, not2_out, adder1_out, shift1_out, shift2_out;
  sc_signal<sc_uint<32> > multi1_out, comp1_out2, and1_out2, nand1_out2, or1_out2, nor1_out2, xor1_out2, xnor1_out2, not1_out2, not2_out2, adder1_out2, shift1_out2, 				shift2_out2, zeros;
  
  // Components instantiation
  add_incr add1;
  Multi multi1;
  Comp comp1;
  and_gate and1;
  nand_gate nand1;
  or_gate or1;
  nor_gate nor1;
  xor_gate xor1;
  xnor_gate xnor1;
  NOT_gate not1;
  NOT_gate not2;
  SR shift1;
  SR shift2;
  bit_extend be1, be2, be3, be4, be5, be6, be7, be8, be9, be10, be11, be12;
  mux16 mux16_1;

  alu2(sc_module_name name);  
};

#endif

