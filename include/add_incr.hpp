#ifndef add_incr_HPP
#define add_incr_HPP

#include "adder.hpp"	// ARITHMETIC 


#include "mux2.hpp"	// MUX to select the operation


using namespace std;


SC_MODULE(add_incr) {
  
  // add_incr entity
  sc_in<sc_uint<16> > add_incr_in1, add_incr_in2;
  sc_in<bool>  add_incr_op1, add_incr_op2;
  sc_out<sc_uint<16> > add_incr_out;


  // Internal signals
  sc_signal<sc_uint<16> > number_one, mux1_out, mux2_out;

  
  // Components instantiation
  adder add1;
  mux2 mux2_1;
  mux2 mux2_2;

  add_incr(sc_module_name name);  
};

#endif

