#ifndef ALU_HPP
#define ALU_HPP

#include "adder.hpp"	// ARITHMETIC 

#include "comparator.hpp"	// and LOGIC	
#include "and_gate.hpp"	        //
#include "or_gate.hpp"		//	
#include "nand_gate.hpp"	//
#include "nor_gate.hpp"		//
#include "xor_gate.hpp"		//
#include "xnor_gate.hpp"	// UNIT

#include "mux8.hpp"	// MUX to select the output


using namespace std;


SC_MODULE(alu) {
  
  // ALU entity
  sc_in<sc_uint<16> > alu_in1, alu_in2;
  sc_in<sc_uint<3> > alu_op;
  sc_out<sc_uint<16> > alu_out;


  // Internal signals
  sc_signal<sc_uint<16> > add1_out, comp1_out, and1_out, nand1_out, or1_out, nor1_out, xor1_out, xnor1_out;

  
  // Components instantiation
  adder add1;
  comparator comp1;
  and_gate and1;
  nand_gate nand1;
  or_gate or1;
  nor_gate nor1;
  xor_gate xor1;
  xnor_gate xnor1;
  mux8 mux8_1;

  alu(sc_module_name name);  
};

#endif

