#ifndef and_gate_HPP
#define and_gate_HPP

SC_MODULE(and_gate) {
  
  sc_in<sc_uint<16> > and_gate_in1;
  sc_in<sc_uint<16> > and_gate_in2;
  sc_out<sc_uint<16> > and_gate_out;
  
  

  SC_CTOR(and_gate) {
	SC_THREAD(and_gate_perform);     
	sensitive << and_gate_in1 << and_gate_in2;
     }

  
  void and_gate_perform();

};

#endif
