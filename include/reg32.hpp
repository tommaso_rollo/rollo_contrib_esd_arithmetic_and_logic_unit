#ifndef reg32_HPP
#define reg32_HPP

SC_MODULE(reg32) {
  
  sc_in<sc_uint<16> > reg32_in;
  sc_in<bool>  reg32_load, reg32_clk;
  sc_out<sc_uint<16> > reg32_out;
  
  

  SC_CTOR(reg32) {
	SC_THREAD(reg32_perform);     
	sensitive << reg32_in << reg32_load << reg32_clk.pos();
     }

  
  void reg32_perform();

};

#endif
