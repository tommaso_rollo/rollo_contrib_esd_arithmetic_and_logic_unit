#ifndef xnor_gate_HPP
#define xnor_gate_HPP

SC_MODULE(xnor_gate) {
  
  sc_in<sc_uint<16> > xnor_gate_in1;
  sc_in<sc_uint<16> > xnor_gate_in2;
  sc_out<sc_uint<16> > xnor_gate_out;
  
  

  SC_CTOR(xnor_gate) {
	SC_THREAD(xnor_gate_perform);     
	sensitive << xnor_gate_in1 << xnor_gate_in2;
     }

  
  void xnor_gate_perform();

};

#endif
