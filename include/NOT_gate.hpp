// NOT logic port

#ifndef NOT_gate_HPP
#define NOT_gate_HPP

#define NBIT 16

SC_MODULE(NOT_gate){
	
	sc_in<sc_uint<NBIT> > NOT_input;
	sc_out<sc_uint<NBIT> > NOT_output;
	

	SC_CTOR(NOT_gate){
		SC_THREAD(notgate_operate);
		sensitive << NOT_input;}

	void notgate_operate();
};

#endif
