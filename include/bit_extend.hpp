#ifndef bit_extend_HPP
#define bit_extend_HPP

SC_MODULE(bit_extend) {
  
  sc_in<sc_uint<16> > bit_extend_in;
  sc_out<sc_uint<32> > bit_extend_out;
  
  

  SC_CTOR(bit_extend) {
	SC_THREAD(bit_extend_perform);     
	sensitive << bit_extend_in;
     }

  
  void bit_extend_perform();

};

#endif
